// iteracion 4
// 1.1 Multiplica 10 por 5 y muestra el resultado mediante alert.
 

const numeroUno = 10
const numeroDos = 5
const numeroTres = 2
const numeroCuatro = 15
const numeroCinco = 9

let producto = numeroUno * numeroDos

alert(producto)
// 1.2 Divide 10 por 2 y muestra el resultado en un alert.
let division = numeroUno/numeroTres
alert(division)
// 1.3 Muestra mediante un alert el resto de dividir 15 por 9.
let resto = numeroCuatro%numeroCinco
alert(resto)
// 1.4 Usa el correcto operador de asignación que resultará en x = 15, 
// teniendo dos variables y = 10 y z = 5.
let x = 0
let y = 10
let z = 5
x = y + z
console.log(x);

// 1.5 Usa el correcto operador de asignación que resultará en x = 50,
// teniendo dos variables y = 10 y z = 5.
x = y * z
console.log(x);