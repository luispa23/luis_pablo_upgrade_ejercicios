const placesToTravel = [{
    id: 5,
    name: 'Japan'
  },
  {
    id: 11,
    name: 'Venecia'
  },
  {
    id: 23,
    name: 'Murcia'
  },
  {
    id: 40,
    name: 'Santander'
  },
  {
    id: 44,
    name: 'Filipinas'
  },
  {
    id: 59,
    name: 'Madagascar'
  }
]


placesToTravel.forEach((lugar, index, object) => {
  if (lugar.id === 11 || lugar.id === 40) {
    object.splice(index, 1)
  }
})
console.log(placesToTravel);