//Crea una función que reciba por parámetro un array y cuando es un valor number lo sume y de lo contrario cuente la longitud del string y lo sume. Puedes usar este array para probar tu función:

// *Iteración #5: Calcular promedio de strings*

// Crea una función que reciba por parámetro un array
//  y cuando es un valor number lo sume y de lo contrario
// cuente la longitud del string y lo sume. Puedes usar este array para probar tu función:


const mixedElements = [6, 1, 'Rayo', 1, 'vallecano', '10', 'upgrade', 8, 'hub'];
let sumaLongString = []
let sumaTotalNumeros = []

    for (let h = 0; h < mixedElements.length; h++) {
        
        const elementos = mixedElements[h];
        if(typeof elementos ==='number') {
            sumaTotalNumeros.push(elementos)
        }else{
            
          sumaLongString.push(elementos.length)
        }
    }

let sumaString = 0
let sumaNumeros = 0
sumaLongString.forEach((item)=>{
    sumaString += item
})
sumaTotalNumeros.forEach((j)=>{
    sumaNumeros += j
})
console.log(sumaString);
console.log(sumaNumeros);