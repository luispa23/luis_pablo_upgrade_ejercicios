/**
 * **Iteración #6: Función swap**

Crea una función llamada `swap()` que reciba un array y dos parametros que sean indices del array.
 La función deberá intercambiar la posición de los valores de los indices que hayamos enviado como parametro. 
Retorna el array resultante.
 */
const noMix = ['Mesirve', 'Cristiano Romualdo', 'Fernando Muralla', 'Ronalguiño'];

function swap(){
[noMix[2], noMix[3]]=[noMix[3],noMix[2]];
return noMix;
}
console.log(swap());
